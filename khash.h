// 
// FILE            khash.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KHASH_KHASH_H
#define KHASH_KHASH_H

#include "kalloc/kaAlloc.h"                            // kaAlloc



// -----------------------------------------------------------------------------
//
// KHashCodeFunction - user defined function that calculates the hash code
//
typedef unsigned int (*KHashCodeFunction)(const char* name);



// -----------------------------------------------------------------------------
//
// KHashCompareFunction - 
//
typedef int (*KHashCompareFunction)(const char* name, void* itemP);



// -----------------------------------------------------------------------------
//
//  KHashListItem -
//
typedef	struct KHashListItem
{
  void*                  data;
  struct KHashListItem*  next;
} KHashListItem;



// -----------------------------------------------------------------------------
//
// KHashTable - 
//
typedef struct KHashTable
{
  KHashListItem**       array;
  int                   arraySize;
  KHashCodeFunction     hashCodeFunction;
  KHashCompareFunction  compareFunction;
  KAlloc*               kallocP;
} KHashTable;



// -----------------------------------------------------------------------------
//
// khashTableCreate
//
extern KHashTable* khashTableCreate(KAlloc* kaP, KHashCodeFunction hashFunction, KHashCompareFunction compareFunction, int slots);



// -----------------------------------------------------------------------------
//
// khashItemAdd
//
extern int khashItemAdd(KHashTable* hashTable, const char* itemName, void* itemData);



// -----------------------------------------------------------------------------
//
// khashItemLookup
//
extern void* khashItemLookup(KHashTable* hashTable, const char* itemName);



// -----------------------------------------------------------------------------
//
// khashItemCustomLookup
//
extern void* khashItemCustomLookup(KHashTable* hashTableP, const char* itemName, KHashCompareFunction compareFunction);



// -----------------------------------------------------------------------------
//
// khashItemLookupInAllSlots - temporal function ...
//
extern void* khashItemLookupInAllSlots(KHashTable* hashTableP, const char* itemName, KHashCompareFunction compareFunction);



// -----------------------------------------------------------------------------
//
// khashRelease
//
extern void khashRelease(KHashTable* hashTableP);

#endif  // KHASH_KHASH_H
