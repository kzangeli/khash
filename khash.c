// 
// FILE            khash.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <stdlib.h>                                    // malloc, NULL
#include <strings.h>                                   // bzero

#include "kalloc/kaAlloc.h"                            // kaAlloc

#include "khash/khash.h"                               // Own interface



// -----------------------------------------------------------------------------
//
// khashTableCreate -
//
KHashTable* khashTableCreate(KAlloc* kaP, KHashCodeFunction hashFunction, KHashCompareFunction compareFunction, int slots)
{
  KHashTable* hashTableP = (KHashTable*) ((kaP != NULL)? kaAlloc(kaP, sizeof(KHashTable)) : malloc(sizeof(KHashTable)));

  if (hashTableP == NULL)
    return NULL;

  hashTableP->array = (KHashListItem**) ((kaP != NULL)? kaAlloc(kaP, slots * sizeof(KHashListItem*)) : malloc(slots * sizeof(KHashListItem*)));
  if (hashTableP->array == NULL)
  {
    if (kaP == NULL)
      free(hashTableP);
    return NULL;
  }

  hashTableP->array[0] = NULL;

  hashTableP->arraySize        = slots;
  hashTableP->hashCodeFunction = hashFunction;
  hashTableP->compareFunction  = compareFunction;
  hashTableP->kallocP          = kaP;

  return hashTableP;
}



// -----------------------------------------------------------------------------
//
// khashItemAdd
//
int khashItemAdd(KHashTable* hashTableP, const char* itemName, void* itemData)
{
  KHashListItem*  itemP;
  int             slot;

  if (hashTableP->kallocP != NULL)
    itemP = (KHashListItem*) kaAlloc(hashTableP->kallocP, sizeof(KHashListItem));
  else
    itemP = (KHashListItem*) malloc(sizeof(KHashListItem));

  if (itemP == NULL)
    return -1;

  itemP->data = itemData;

  // Get the slot of the hash table
  slot = hashTableP->hashCodeFunction(itemName) % hashTableP->arraySize;

  // Insert the item in its slot (as the first item of the list)
  itemP->next = hashTableP->array[slot];
  hashTableP->array[slot] = itemP;

  return 0;
}



// -----------------------------------------------------------------------------
//
// khashItemLookup
//
void* khashItemLookup(KHashTable* hashTableP, const char* itemName)
{
  unsigned int slot;

  // Get the slot of the hash table
  slot = hashTableP->hashCodeFunction(itemName) % hashTableP->arraySize;

  KHashListItem* itemP;
  for (itemP = hashTableP->array[slot]; itemP != NULL; itemP = itemP->next)
  {
    if (hashTableP->compareFunction(itemName, itemP->data) == 0)
      return itemP->data;
  }

  return NULL;
}



// -----------------------------------------------------------------------------
//
// khashItemCustomLookup
//
void* khashItemCustomLookup(KHashTable* hashTableP, const char* itemName, KHashCompareFunction compareFunction)
{
  int slot;

  // Get the slot of the hash table
  slot = hashTableP->hashCodeFunction(itemName) % hashTableP->arraySize;

  KHashListItem* itemP;
  for (itemP = hashTableP->array[slot]; itemP != NULL; itemP = itemP->next)
  {
    if (compareFunction(itemName, itemP->data) == 0)
      return itemP->data;
  }

  return NULL;
}



// -----------------------------------------------------------------------------
//
// khashItemLookupInAllSlots - temporal function ...
//
void* khashItemLookupInAllSlots(KHashTable* hashTableP, const char* itemName, KHashCompareFunction compareFunction)
{
  int slot;
  for (slot = 0; slot < hashTableP->arraySize; ++slot)
  {
    KHashListItem* itemP;
    for (itemP = hashTableP->array[slot]; itemP != NULL; itemP = itemP->next)
    {
      if (compareFunction(itemName, itemP->data) == 0)
        return itemP->data;
    }
  }

  return NULL;
}



// -----------------------------------------------------------------------------
//
// khashRelease
//
void khashRelease(KHashTable* hashTableP)
{
  if (hashTableP->kallocP == NULL)
  {
    int ix;
    for (ix = 0; ix < hashTableP->arraySize; ix++)
    {
      if (hashTableP->array[ix] != NULL)
      {
        KHashListItem* itemP;
        for (itemP = hashTableP->array[ix]; itemP != NULL; itemP = itemP->next)
          free(itemP);

        free(hashTableP->array[ix]);
      }
    }

    free(hashTableP);
  }
}
