# khash - simple hash table

## Table of Contents

## Background
The Orion-LD context broker needed a *hash table* to store its @contexts in a ways that would give faster lookups.
A hash-table is common stuff and I foresee that I will reuse this library in future projects, so, instead of making it a part of Orion-LD,
I decided to take out a new independent repo for this.

## Install
% make di  # debug install

## Usage
The test progam `khashTest.c` that was used during implementation of this simple library:

```
#include <stdio.h>                                     // printf
#include <string.h>                                    // strcmp
#include <stdlib.h>                                    // malloc
#include "khash/khash.h"                               // Hash Table

// -----------------------------------------------------------------------------
//
// HashItem -
//
typedef struct HashItem
{
  char* name;
  char* relation;
} HashItem;

// -----------------------------------------------------------------------------
//
// hashCode -
//
int hashCode(const char* name)
{
  int code = 0;

  while (*name != 0)
  {
    code += *name;
    ++name;
  }

  return code;
}

// -----------------------------------------------------------------------------
//
// compareFunction -
//
int compareFunction(const char* name, void* item)
{
  HashItem* itemP = (HashItem*) item;

  return strcmp(name, itemP->name);
}

// -----------------------------------------------------------------------------
//
// dumpHashTable -
//
void dumpHashTable(KHashTable* hashTableP)
{
  for (int slot = 0; slot < hashTableP->arraySize; slot++)
  {
    printf("Slot %03d:  ", slot);
    for (KHashListItem* itemP = hashTableP->array[slot]; itemP != NULL; itemP = itemP->next)
    {
      HashItem* hiP = itemP->data;

      printf("%s -> ", hiP->name);
    }
    printf("NULL\n");
  }
}

// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  KHashTable*  hashTableP  = khashTableCreate(NULL, hashCode, compareFunction, 8);
  const char*  names[]     = { "Ken", "Malika", "Elliott", "Maria", "Alex", "Gun", "Dan",
                               "Pia", "Jonathan", "Isabelle", "Jocke",
                               "Gunnar", "Evelina", "Hasse", "Lisa", "Stig", "Britt-Marie", "Camilla", "Kenneth", NULL };
  const char*  relations[] = { "Myself", "Wife", "Second Son", "Daughter", "First Son", "Mother", "Father",
                               "Sister", "Niece 2", "Niece 1", "Ex Brother In Lax",
                               "Grandfather", "Grandmother", "Uncle H", "Aunt L", "Uncle S", "Aunt B", "Cousin C", "Cousin K", NULL };
  int ix = 0;

  while (names[ix] != NULL)
  {
    HashItem* itemP = (HashItem*) malloc(sizeof(HashItem));

    itemP->name     = (char*) names[ix];
    itemP->relation = (char*) relations[ix];
                                         
    khashItemAdd(hashTableP, itemP->name, itemP);
    ++ix;
  }

  if (strcmp(argV[1], "table") == 0)
  {
    dumpHashTable(hashTableP);
    return 0;
  }

  HashItem* itemP = (HashItem*) khashItemLookup(hashTableP, argV[1]);

  if (itemP == NULL)
    printf("Can't find '%s' in hash table\n", argV[1]);
  else
    printf("Found '%s', my %s\n", itemP->name, itemP->relation);

  return 0;
}

```

## License
[Apache 2.0](LICENSE) © 2019-2020 Ken Zangelin
