// 
// FILE            khashTest.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                                     // printf
#include <string.h>                                    // strcmp
#include <stdlib.h>                                    // malloc

#include "khash/khash.h"                               // Hash Table



// -----------------------------------------------------------------------------
//
// HashItem -
//
typedef struct HashItem
{
  char* name;
  char* relation;
} HashItem;



// -----------------------------------------------------------------------------
//
// hashCode -
//
unsigned int hashCode(const char* name)
{
  unsigned int code = 0;

  while (*name != 0)
  {
    code += (unsigned char) *name;
    ++name;
  }

  return code;
}



// -----------------------------------------------------------------------------
//
// compareFunction -
//
int compareFunction(const char* name, void* item)
{
  HashItem* itemP = (HashItem*) item;

  return strcmp(name, itemP->name);
}






// -----------------------------------------------------------------------------
//
// dumpHashTable -
//
void dumpHashTable(KHashTable* hashTableP)
{
  int slot;
  for (slot = 0; slot < hashTableP->arraySize; slot++)
  {
    KHashListItem* itemP;
    printf("Slot %03d:  ", slot);
    for (itemP = hashTableP->array[slot]; itemP != NULL; itemP = itemP->next)
    {
      HashItem* hiP = itemP->data;

      printf("%s -> ", hiP->name);
    }
    printf("NULL\n");
  }
}



// -----------------------------------------------------------------------------
//
// main - 
//
int main(int argC, char* argV[])
{
  KHashTable*  hashTableP  = khashTableCreate(NULL, hashCode, compareFunction, 8);
  const char*  names[]     = { "Ken", "Malika", "Elliott", "Maria", "Alex", "Gun", "Dan",
                               "Pia", "Jonathan", "Isabelle", "Jocke",
                               "Gunnar", "Evelina", "Hasse", "Lisa", "Stig", "Britt-Marie", "Camilla", "Kenneth", NULL };
  const char*  relations[] = { "Myself", "Wife", "Second Son", "Daughter", "First Son", "Mother", "Father",
                               "Sister", "Niece 2", "Niece 1", "Ex Brother In Lax",
                               "Grandfather", "Grandmother", "Uncle H", "Aunt L", "Uncle S", "Aunt B", "Cousin C", "Cousin K", NULL };
  int ix = 0;

  while (names[ix] != NULL)
  {
    HashItem* itemP = (HashItem*) malloc(sizeof(HashItem));

    itemP->name     = (char*) names[ix];
    itemP->relation = (char*) relations[ix];
                                         
    khashItemAdd(hashTableP, itemP->name, itemP);
    ++ix;
  }

  if (strcmp(argV[1], "table") == 0)
  {
    dumpHashTable(hashTableP);
    return 0;
  }

  HashItem* itemP = (HashItem*) khashItemLookup(hashTableP, argV[1]);

  if (itemP == NULL)
    printf("Can't find '%s' in hash table\n", argV[1]);
  else
    printf("Found '%s', my %s\n", itemP->name, itemP->relation);

  return 0;
}
